import face_recognition

def recognize_face(image_path):
    image = face_recognition.load_image_file(image_path)
    face_locations = face_recognition.face_locations(image)
    
    if len(face_locations) > 0:
        return "Wajah teridentifikasi!"
    else:
        return "Tidak ada wajah yang teridentifikasi."

if __name__ == "__main__":
    import sys
    image_path = sys.argv[1]
    result = recognize_face(image_path)
    print(result)
