<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**    
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'password_confirmation' => 'required|same:password',
        ]);
   
        if($validator->fails()) {
            $response = [
                'success' => false,
                'message' => 'Validation Error.',
            ];
    
            if(!empty($errorMessages)) {
                $response['data'] = $validator->errors();
            }
    
            return response()->json($response, 404);
        }
   
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')->plainTextToken;
        $success['name'] =  $user->name;
        
        $response = [
            'success' => true,
            'data'    => $success,
            'message' => 'User register successfully.',
        ];

        return response()->json($response, 200);
    }
   
    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request): JsonResponse
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) { 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->plainTextToken; 
            $success['name'] =  $user->name;
            
            $response = [
                'success' => true,
                'data'    => $success,
                'message' => 'User login successfully.',
            ];
    
            return response()->json($response, 200);
        } else { 
            $response = [
                'success' => false,
                'message' => 'Unauthorised.',
            ];
    
            if(!empty($errorMessages)) {
                $response['data'] = ['error'=>'Unauthorised'];
            }
    
            return response()->json($response, 404);
        } 
    }
}
