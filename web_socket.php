<?php
    $server = new WebSocketServer();

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $inputData = file_get_contents('php://input');
        $result = processData($inputData);

        header('Content-Type: application/json');
        echo json_encode(['result' => $result]);
    }

    function processData($data) {
        return strtoupper($data);
    }

    class WebSocketServer {
        private $clients = [];

        public function __construct() {
            $socket = stream_socket_server("tcp://localhost:8080", $errno, $errstr);
            if (!$socket) {
                die("$errstr ($errno)");
            }

            while ($conn = stream_socket_accept($socket, -1)) {
                $this->clients[] = $conn;
                $this->handleClient($conn);
            }
        }

        private function handleClient($conn) {
            echo "Client connected\n";

            while ($data = fread($conn, 1024)) {
                $this->broadcast($data);
            }

            fclose($conn);
            $this->clients = array_diff($this->clients, [$conn]);
            echo "Client disconnected\n";
        }

        private function broadcast($data) {
            foreach ($this->clients as $client) {
                fwrite($client, $data);
            }
        }
    }
?>