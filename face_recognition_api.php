<?php

    function identifyFace($imageData) {
        $imagePath = 'path_to_save_image.jpg';
        file_put_contents($imagePath, base64_decode($imageData));

        $output = shell_exec('python face_recognition_script.py ' . $imagePath);
        return $output;
    }

    $inputData = file_get_contents('php://input');

    $result = identifyFace($inputData);

    header('Content-Type: application/json');
    echo json_encode(['result' => $result]);
?>
